package com.example.mindaugas.checkyourbill;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "DBHelper";

    private static final String DATABASE_NAME = "BankSystem.db";
    private static final int DATABASE_VERSION = 1;

    //Client
    private static final String CLIENT_TableName = "Client";
    private static final String CLIENT_ID = "ID";
    private static final String CLIENT_FirstName = "FirstName";
    private static final String CLIENT_LastName = "LastName";

    //Account
    private static final String Account_Tablename = "Account";
    private static final String Account_IBAN = "IBAN";
    private static final String Account_balance = "Balance";
    private static final String Account_OwnerID = "Owner";

    //Transaction
    private static final String Transaction_Tablename = "Transaction";
    private static final String Transaction_ID = "ID";
    private static final String Transaction_creditAccount = "creditAccount";
    private static final String Transaction_debitAccount = "debitAccount";
    private static final String Transaction_creditAmount = "creditAmount";
    private static final String Transaction_debitAmount = "debitAmount";
    private static final String Transaction_transactionDate = "Transaction Date";

    //CREATE TABLE CLIENT
    private static final String SQL_CREATE_TABLE_CLIENT = "CREATE TABLE " + CLIENT_TableName + "("
            + CLIENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CLIENT_FirstName + " VARCHAR(255) NOT NULL, "
            + CLIENT_LastName + " VARCHAR(255) NOT NULL "
            + ");";

    //CREATE TABLE ACCOUNT
    private static final String SQL_CREATE_TABLE_ACCOUNT = "CREATE TABLE " + Account_Tablename + "("
            + Account_IBAN + " CHAR(34) PRIMARY KEY NOT NULL, "
            + Account_balance + " FLOAT NOT NULL, "
            + Account_OwnerID + " INTEGER, "
            + "FOREIGN KEY(" + Account_OwnerID + ") REFERENCES " + CLIENT_TableName + "(" + CLIENT_ID + ")"
            + ");";

    //CREATE TABLE TRANSACTION
    private static final String SQL_CREATE_TABLE_TRANSACTION = "CREATE TABLE " + Transaction_Tablename + "("
            + Transaction_ID + " INTEGER AUTOINCREMENT, "
            + Transaction_creditAccount + " CHAR(34), "
            + Transaction_debitAccount + " CHAR(34), "
            + Transaction_creditAmount + " FLOAT NOT NULL, "
            + Transaction_debitAmount + " FLOAT NOT NULL, "
            + Transaction_transactionDate + " TIMESTAMP, "
            + "FOREIGN KEY(" + Transaction_creditAccount + ") REFERENCES " + Account_Tablename + "(" + Account_IBAN + "), "
            + "FOREIGN KEY(" + Transaction_debitAccount + ") REFERENCES " + Account_Tablename + "(" + Account_IBAN + ")"
            + ");";

    //INSERT BUILT_IN VALUES
    //CLIENTS
    //CLIENT_VALUE1
    private static final String SQL_INSERT_STATIC_CLIENT_VALUES_1 = " INSERT INTO " + CLIENT_TableName + "("
            + CLIENT_FirstName + ", "
            + CLIENT_LastName + ") VALUES ('Mindaugas', 'Samsonas');";

    //CLIENT_VALUE2
    private static final String SQL_INSERT_STATIC_CLIENT_VALUES_2 = " INSERT INTO " + CLIENT_TableName + "("
            + CLIENT_FirstName + ", "
            + CLIENT_LastName + ") VALUES ('Jonas', 'Petrauskas');";

    //ACCOUNTS
    //ACCOUNT_VALUE1
    private static final String SQL_INSERT_STATIC_ACCOUNT_VALUES_1 = " INSERT INTO " + Account_Tablename + "("
            + Account_IBAN + ", "
            + Account_balance + ", "
            + Account_OwnerID + ") VALUES ('5167-4475-2784-6951', '500', '1');";
    //ACCOUNT_VALUE2
    private static final String SQL_INSERT_STATIC_ACCOUNT_VALUES_2 = " INSERT INTO " + Account_Tablename + "("
            + Account_IBAN + ", "
            + Account_balance + ", "
            + Account_OwnerID + ") VALUES ('5391-2321-5266-6553', '100', '2');";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
    db.execSQL(SQL_CREATE_TABLE_CLIENT);
    db.execSQL(SQL_CREATE_TABLE_ACCOUNT);
    db.execSQL(SQL_CREATE_TABLE_TRANSACTION);
    db.execSQL(SQL_INSERT_STATIC_CLIENT_VALUES_1);
    db.execSQL(SQL_INSERT_STATIC_CLIENT_VALUES_2);
    db.execSQL(SQL_INSERT_STATIC_ACCOUNT_VALUES_1);
    db.execSQL(SQL_INSERT_STATIC_ACCOUNT_VALUES_2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + CLIENT_TableName);
    db.execSQL("DROP TABLE IF EXISTS " + Account_Tablename);
    //db.execSQL("DROP TABLE IF EXISTS " + Transaction_Tablename);
        onCreate(db);
    }

    public Cursor Check_If()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor gettingFullName = checkName();
        String[] gettingFullNameString = gettingFullName.toString().split(" ");
        String gettingName = gettingFullNameString[0];

    }

    public Cursor checkName(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT "+ CLIENT_FirstName + "," + CLIENT_LastName + " FROM " + CLIENT_TableName, null);
        return res;
    }

    public Cursor check_psw(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor resu = db.rawQuery("SELECT " + Account_IBAN + " FROM "+ Account_Tablename + " WHERE " + Account_OwnerID + " = " + CLIENT_ID, null);
        return resu;
    }
}
